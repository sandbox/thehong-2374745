<?php

use Drupal\at_hipchat\HookImplementation\HookRulesActionInfo;

/**
 * @file
 * Rules integration.
 */

/**
 * Implements hook_rules_action_info().
 */
function at_hipchat_rules_action_info() {
  $obj = new HookRulesActionInfo();
  return $obj->execute();
}
