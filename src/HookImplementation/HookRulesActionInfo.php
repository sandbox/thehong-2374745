<?php

namespace Drupal\at_hipchat\HookImplementation;

use GorkaLaucirica\HipchatAPIv2Client\Model\Message;

class HookRulesActionInfo {

  public function execute() {
    return array(
        'at_hipchat_send' => $this->getRoomNotifyAction(),
    );
  }

  private function getRoomNotifyAction() {
    $action = array(
        'label'     => t('Send message to room'),
        'group'     => t('HipChat'),
        'parameter' => array(),
    );

    // The room the message should be sent to.
    $action['parameter']['room'] = array(
        'type'        => 'textfield',
        'label'       => t('Room'),
        'description' => t("Leave empty for default room"),
        'optional'    => true,
    );

    // From
    $action['parameter']['from'] = array(
        'type'        => 'text',
        'label'       => t('From'),
        'description' => t('Sender name'),
    );

    // The message to be sent
    $action['parameter']['message'] = array(
        'type'        => 'text',
        'label'       => t('Message'),
        'description' => t('The message body.'),
    );

    // Message color
    $action['parameter']['color'] = array(
        'type'    => 'selelect',
        'label'   => t('Color'),
        'options' => array(
            Message::COLOR_GRAY   => t('Gray'),
            Message::COLOR_GREEN  => t('Green'),
            Message::COLOR_PURPLE => t('Purple'),
            Message::COLOR_RED    => t('Red'),
            Message::COLOR_YELLOW => t('Yellow'),
            Message::COLOR_RANDOM => t('Random'),
        ),
    );

    return $action;
  }

}
