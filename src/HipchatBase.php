<?php

namespace Drupal\at_hipchat;

use GorkaLaucirica\HipchatAPIv2Client\API\RoomAPI;
use GorkaLaucirica\HipchatAPIv2Client\API\UserAPI;
use GorkaLaucirica\HipchatAPIv2Client\Auth\OAuth2;
use GorkaLaucirica\HipchatAPIv2Client\Client;

/**
 * Wrapper to hipchat services.
 */
abstract class HipchatBase {

  /** @var UserAPI */
  private $userAPI;

  /** @var RoomAPI */
  private $roomAPI;

  /** @var OAuth2 */
  private $oAuth2;

  /** @var Client */
  private $client;

  /** @var string */
  private $authToken;

  public function __construct($authToken) {
    $this->authToken = $authToken;
  }

  public function getUserAPI() {
    if (NULL === $this->userAPI) {
      $client = $this->getClient();
      $this->userAPI = new UserAPI($client);
    }
    return $this->userAPI;
  }

  public function getRoomAPI() {
    if (NULL === $this->roomAPI) {
      $client = $this->getClient();
      $this->roomAPI = new RoomAPI($client);
    }
    return $this->roomAPI;
  }

  public function getOauth2() {
    if (NULL === $this->oAuth2) {
      $this->oAuth2 = new OAuth2($this->authToken);
    }
    return $this->oAuth2;
  }

  public function getClient() {
    if (NULL === $this->client) {
      $auth = new OAuth2($this->authToken);
      $this->client = new Client($auth);
    }
    return $this->client;
  }

  public function setUserAPI($userAPI) {
    $this->userAPI = $userAPI;
    return $this;
  }

  public function setRoomAPI($roomAPI) {
    $this->roomAPI = $roomAPI;
    return $this;
  }

  public function setOauth2($oauth2) {
    $this->oAuth2 = $oauth2;
    return $this;
  }

  public function setClient($client) {
    $this->client = $client;
    return $this;
  }

}
