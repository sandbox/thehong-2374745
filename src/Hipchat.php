<?php

namespace Drupal\at_hipchat;

use Drupal\at_hipchat\HipchatBase;
use GorkaLaucirica\HipchatAPIv2Client\Model\Message;

class Hipchat extends HipchatBase {

  const MESSAGE_CLASS = 'GorkaLaucirica\HipchatAPIv2Client\Model\Message';

  public function createMessage($from, $msg, $color = Message::COLOR_YELLOW, $message_format = Message::FORMAT_HTML, $notify = FALSE) {
    $class_name = static::MESSAGE_CLASS;
    $message = new $class_name;
    $message->parseJson(array(
        'from'           => $from,
        'message'        => $msg,
        'color'          => $color,
        'message_format' => $message_format,
    ));
    return $message->setNotify($notify);
  }

}
