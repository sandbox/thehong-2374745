<?php

/**
 * Implements hook_entity_info().
 */
function at_hipchat_entity_info() {
  $info = array();

  $info['hipchat_account'] = array(
      'label'            => t('Hipchat account'),
      'entity class'     => 'Drupal\at_hipchat\Entity\Account',
      'controller class' => 'Drupal\at_hipchat\Entity\AccountController',
      'base table'       => 'at_hipchat_account',
      'fieldable'        => FALSE,
      'entity keys'      => array(
          'id'    => 'id',
          'label' => 'title'
      ),
      'access callback'  => 'at_hipchat_access_callback',
      'module'           => 'at_hipchat',
      'bundles'          => array(),
      'admin ui'         => array(
          'path'             => 'admin/config/people/at-hipchat',
          'file'             => 'at_hipchat.pages.inc',
          'controller class' => 'Drupal\at_hipchat\Entity\AccountUIController',
      ),
  );

  return $info;
}
